# Rest Manager 

Android library for quick configuration of rest API (Using retrofit and OKHttp3)

Current version: `0.9`

```groovy
implementation 'pl.bms:network:0.9'
```
minSdkVersion **15**

## Configuration

First you need to use RestBuilder to create ReastManager:

The simplest implementation:
```kotlin
val restManager = RestBuilder(context)
	.setBaseUrl("https://api.example.com")
	.build()
```
You can setup timeouts: (defaults are **30s write** and **30s read**)
```kotlin
	.setupTimeouts(30, 30, TimeUnit.SECONDS)
```
Logging level with options:
- NONE: No logs. (default)
- BASIC: Logs request and response lines.
- HEADERS: Logs request and response lines and their respective headers.
- BODY: Logs request and response lines and their respective headers and bodies (if present).
```kotlin
	.setLoggingLevel(RestBuilder.LoggingLevel.HEADERS)
```
Set map of headers:
```kotlin
	.setHeaders(mapOf("Header" to "ExampleValue", "SecondHeade" to "SecondValue"))
```
Token is a header that you can refresh any time by calling `restManager.refreshToken(newToken)`
First parameter is token header key. Second is his format 
```kotlin
	.setTokenHeader("Authorization", "Bearer %s")
```
You can add your own `okhttp3.Interceptor`
```kotlin
	.addInterceptor(YourCustomInterceptor())
```
You can configure the manager  to trust each certificate
```kotlin
	.trustAllCertificates(true)
```
Or provide your own to be trusted
```kotlin
	.setCertificate(certificate)
```
You can add type adapters (with must implement at least one of the interfaces: TypeAdapter, InstanceCreator, JsonSerializer, JsonDeserializer from com.google.gson package)
```kotlin
	.addGsonTypeAdapter(Date::class.java, DateSerializer())
```
If you need to configure the Gson object more precisely you should use **setGson** method
```kotlin
	.setGson(gson: Gson)
```

You can also add error handlers for HttpErrors and NetworkErrors. 
```kotlin
	.handleHttpErrors(httpErrorHandler)
	.handleNetworkErrors(networkErrorHandler)
```

## Usage

# ApiProvider
You need to create interface same as in retrofit ( http://square.github.io/retrofit/ ):
```kotlin
interface GitHubService {
  @GET("users/{userId}")
  fun getUser(@Path("user") id: Long): Single<Response<User>> 
}
```
Then you can just get api from ApiProvider (It will create or return Api created before) :
```kotlin
val api = restManager.apiProvider.getApi(GitHubService::class.java)

api.getUser(userId)
    .subscribe (
        { response ->
            //TODO: use response
        },
        { error ->
            //TODO: handle IOException 
        }
    )
```

# Refresh token
You can refresh the token that is sent in the headers whenever you want, using the refreshToken()
```kotlin
restManager.refreshToken(newToken)  // user logged in - refresh token
restManager.refreshToken(null)  // user logout - remove token
```

# Error converter
You can covert response body to specific error class with ErrorConverter:
```kotlin
val error: ValidationError? = restManager.errorConverter.convertError(ValidationError::class.java, response)
```
It return ValidationError if response can be converted, or null if can't. 


## Error handling
You can globally handle errors for all rest calls. You can handle 2 kinds of errors: HttpError and NetworkError.
# HttpError
HttpErrors are created when the request is successfully sent, but received a response with a code other than 200-299. 
HttpError class has type of error:
```kotlin
enum class Type {
        REDIRECT, //code: 300-399
        VALIDATION, // code: 400
        AUTHORIZATION, // code: 401
        FORBIDDEN, // code: 403
        NOT_FOUND, // code: 404
        GENERAL, //code: 402, 405-499
        SERVER, //code: 500-599
        UNKNOWN //else
    }
```
end requestType:
```kotlin
enum class RequestType {
        READ, // request method is GET or HEAD
        WRITE // else
    }
```
From HttpError you can get also: code, message, body, request 

# NetworkError
NetworkError are created when the request was not sent because of the IOException. 
Even if network errors are handled globally, you still need to handle IOException on every call to avoid application crashes. (In RxJava calls you need to add onError block to all subscribe() methods).
NetworkError class has reason of error:
```kotlin
enum class Reason {
        NO_INTERNET,
        NO_CONNECTION,
        TIMEOUT,
        CANCELED,
        UNKNOWN
    }
```
end requestType same as HttpError:
```kotlin
enum class RequestType {
        READ, // request method is GET or HEAD
        WRITE // else
    }
```


## Used dependecies
```groovy
    org.jetbrains.kotlin:kotlin-stdlib-jre7:$kotlin_version
	com.android.support:appcompat-v7:$support_version
    com.squareup.retrofit2:retrofit:$retrofit_version
    com.squareup.retrofit2:converter-gson:$retrofit_version
    com.squareup.retrofit2:adapter-rxjava2:$retrofit_version
    com.squareup.okhttp3:logging-interceptor:$okhttp_logging_interceptor_version
    io.reactivex.rxjava2:rxjava:$rx_version
    io.reactivex.rxjava2:rxandroid:$rxandroid_version
```

#### Dependecies verions 

```groovy
	kotlin_version = '1.2.21'
    support_version = '26.1.0'
    retrofit_version = '2.3.0'
    okhttp_logging_interceptor_version = '3.8.1'
    rx_version = '2.1.8'
    rxandroid_version = '2.0.1'
```

## License
	Copyright 2018 BM Solution & Soft-AB

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.

