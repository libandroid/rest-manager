package pl.bms.restmanagersample

import android.app.Application
import android.nfc.Tag
import android.util.Log
import okhttp3.Interceptor
import pl.bms.network.RestBuilder
import pl.bms.network.error.HttpError
import pl.bms.network.error.NetworkError
import pl.bms.restmanagersample.serializer.DateSerializer
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 20.02.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class NetworkApplication: Application() {

    private val TAG = this::class.java.simpleName

    var httpErrorHandler: ((HttpError) -> Unit)? = null
    var networkErrorHandler: ((NetworkError) -> Unit)? = null

    val restManager by lazy {
        RestBuilder()
                .setBaseUrl(getString(R.string.api_base_url))
                .addGsonTypeAdapter(Date::class.java, DateSerializer())
                .setLoggingLevel(RestBuilder.LoggingLevel.HEADERS)
                .setupTimeouts(10, 10, TimeUnit.SECONDS)
                .setHeaders(mapOf("CustomHeader" to "Example"))
                .addInterceptor(Interceptor { chain ->
                    val response = chain.proceed(chain.request())
                    Log.d(TAG, "Example of custom interceptor. Response message: ${response.message()}")
                    response
                })
                .handleHttpErrors { httpError ->
                    httpErrorHandler?.invoke(httpError)
                }
                .handleNetworkErrors { error ->
                    networkErrorHandler?.invoke(error)
                }
                .build()
    }

    val apiProvider by lazy {
        restManager.apiProvider
    }

}