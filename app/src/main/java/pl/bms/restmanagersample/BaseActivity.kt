package pl.bms.restmanagersample

import android.support.v7.app.AppCompatActivity
import android.util.Log
import org.jetbrains.anko.alert
import pl.bms.network.error.HttpError
import pl.bms.network.error.NetworkError


/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 20.02.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

abstract class BaseActivity: AppCompatActivity() {

    val TAG: String by lazy { this::class.java.simpleName }
    val app: NetworkApplication by lazy { application as NetworkApplication }

    override fun onResume() {
        super.onResume()
        app.httpErrorHandler = httpErrorHandler
        app.networkErrorHandler = networkErrorHandler
    }

    override fun onPause() {
        app.httpErrorHandler = null
        app.networkErrorHandler = null
        super.onPause()
    }

    private var httpErrorHandler: ((HttpError) -> Unit) = { error ->
        val title = when (error.requestType) {
            HttpError.RequestType.WRITE -> "Cannot save data"
            HttpError.RequestType.READ -> "Cannot load data"
        }
        when (error.type) {
            HttpError.Type.VALIDATION -> {
                // Do nothing, we will handle this kind of errors elsewhere
            }
            HttpError.Type.AUTHORIZATION -> {
                alert("Authorization problems. Remove token!", title).show()
            }
            else -> {
                alert("Http error with code ${error.code} \n${error.message}")
            }
        }
    }

    private var networkErrorHandler: ((NetworkError) -> Unit) = { error ->
        val title = when (error.requestType) {
            NetworkError.RequestType.WRITE -> "Cannot save data"
            NetworkError.RequestType.READ -> "Cannot load data"
        }
        alert("Network error \nreason: ${error.reason}", title).show()
    }

}