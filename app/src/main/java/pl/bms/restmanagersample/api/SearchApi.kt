@file:Suppress("unused")

package pl.bms.restmanagersample.api

import io.reactivex.Single
import pl.bms.networkarchitecture.model.RepoSearchResults
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 01.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
interface SearchApi {

    @GET("/search/repositories")
    fun searchRepositories(@Query("q") searchString: String) : Single<Response<RepoSearchResults>>

    @GET("/search/repositories")
    fun searchRepositoriesCall(@Query("q") searchString: String) : Call<RepoSearchResults>

}