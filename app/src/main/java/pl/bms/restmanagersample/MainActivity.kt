package pl.bms.restmanagersample

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast
import pl.bms.network.error.HttpError
import pl.bms.networkarchitecture.model.RepoSearchResults
import pl.bms.restmanagersample.api.SearchApi
import retrofit2.Response
import java.io.IOException

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 20.02.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class MainActivity : BaseActivity() {

    var disposable: Disposable? = null
    private val adapter: RepositoriesAdapter by lazy { RepositoriesAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupViews()
        initActions()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main_activity, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuitem_set_token -> {
                app.restManager.refreshToken("WrongToken")
            }
            R.id.menuitem_remove_token -> {
                app.restManager.refreshToken(null)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupViews() {
        mainActivityRecyclerView.layoutManager = LinearLayoutManager(this)
        mainActivityRecyclerView.adapter = adapter
    }

    private fun initActions() {
        mainActivitySearchButton.setOnClickListener {
            mainActivitySearchEditText.text.toString().also {
                if (it.length < 3) {
                    toast("Search text must have at least 3 characters")
                } else {
                    search(it)
                }
            }

        }
    }

    private fun search(searchString: String) {
        disposable?.dispose()
        mainActivityProgressBar.visibility = View.VISIBLE
        disposable = app.apiProvider.getApi(SearchApi::class.java)
                .searchRepositories(searchString)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> onGetResult(result) },
                        { error -> onGetError(error) }
                )
    }

    private fun onGetResult(result: Response<RepoSearchResults>) {
        mainActivityProgressBar.visibility = View.GONE
        if (result.isSuccessful) {
            adapter.items = result.body()?.items ?: emptyList()
        } else {
            HttpError(result).apply {
                if (type == HttpError.Type.VALIDATION) {
                    //We can handle validation error here
                    toast("validation error: $message")
                }
            }

        }
    }

    private fun onGetError(error: Throwable) {
        mainActivityProgressBar.visibility = View.GONE
        // Must to have this section. Even if network errors are handled in Base Activity.
        // Because Rx would crash if an error appeared and that section was not there
        if (error is IOException) {
            // Network error is always IOException. It is already handled
            Log.d(TAG, "Network error appeared")
        } else {
            // Unknown error
            throw error
        }
    }

    override fun onPause() {
        disposable?.dispose()
        disposable = null
        super.onPause()
    }


}
