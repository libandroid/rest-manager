@file:Suppress("MemberVisibilityCanBePrivate")

package pl.bms.network.error

import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 04.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
data class HttpError(
        val response: Response
) {

    val code: Int = response.code()
    val message: String? = response.message()
    val body: ResponseBody? = response.body()
    val request: Request = response.request()
    val type: Type
    val requestType: RequestType

    constructor(retrofitResponse: retrofit2.Response<*>) : this (retrofitResponse.raw())

    init {
        type = when (code) {
            in 200..299 -> throw IllegalStateException("Http error cannot be created from successful response")
            400 -> Type.VALIDATION
            401 -> Type.AUTHORIZATION
            in 402..499 -> Type.GENERAL
            in 500..599 -> Type.SERVER
            in 300..399 -> Type.REDIRECT
            else -> Type.UNKNOWN
        }
        val readOnly = request.method().toLowerCase() == "get" || request.method().toLowerCase() == "head"
        requestType = if (readOnly) RequestType.READ else RequestType.WRITE
    }

    enum class RequestType {
        WRITE,
        READ
    }

    enum class Type {
        REDIRECT, //code: 300-399
        VALIDATION, // code: 400
        AUTHORIZATION, // code: 401
        GENERAL, //code: 402-499
        SERVER, //code: 500-599
        UNKNOWN //else
    }

}