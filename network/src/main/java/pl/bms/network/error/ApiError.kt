package pl.bms.network.error


/**
 * Created by Rafał Olszewski on 17.05.18.
 *
 */
class ApiError (
        val statusCode: Int,
        val message: String?,
        val validationErrors: Map<String, String>
)