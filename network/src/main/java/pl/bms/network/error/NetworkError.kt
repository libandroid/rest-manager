@file:Suppress("MemberVisibilityCanBePrivate")

package pl.bms.network.error

import okhttp3.Request
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 09.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
data class NetworkError(val request: Request, val throwable: IOException) {

    val requestType: RequestType
    val reason: Reason

    init {
        val readOnly = request.method().toLowerCase() == "get" || request.method().toLowerCase() == "head"
        requestType = if (readOnly) RequestType.READ else RequestType.WRITE
        reason = when (throwable) {
            is ConnectException -> Reason.NO_INTERNET
            is SocketTimeoutException -> Reason.TIMEOUT
            else -> Reason.UNKNOWN
        }
    }

    enum class RequestType {
        WRITE,
        READ
    }

    enum class Reason {
        NO_INTERNET,
        TIMEOUT,
        UNKNOWN
    }

}