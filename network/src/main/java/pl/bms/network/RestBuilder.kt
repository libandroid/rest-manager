package pl.bms.network

import pl.bms.network.error.HttpError
import pl.bms.network.error.HttpErrorHandler
import pl.bms.network.error.NetworkError
import pl.bms.network.error.NetworkErrorHandler
import com.google.gson.Gson
import io.reactivex.Scheduler
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor
import java.lang.reflect.Type
import java.security.cert.Certificate
import java.util.concurrent.TimeUnit

@Suppress("unused")

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 04.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
class RestBuilder {

    @JvmSynthetic internal var headers: Map<String, String> = emptyMap()
    @JvmSynthetic internal var baseUrl: String = ""
    @JvmSynthetic internal var tokenHeaderField: String = "Authorization"
    @JvmSynthetic internal var tokenHeaderFormat = "Bearer %s"
    @JvmSynthetic internal var loggingLevel = LoggingLevel.NONE
    @JvmSynthetic internal var httpErrorHandler: HttpErrorHandler? = null
    @JvmSynthetic internal var networkErrorHandler: NetworkErrorHandler? = null
    @JvmSynthetic internal var readTimeout: Long = 30
    @JvmSynthetic internal var writeTimeout: Long = 30
    @JvmSynthetic internal var timeoutTimeUnit: TimeUnit = TimeUnit.SECONDS
    @JvmSynthetic internal val applicationInterceptors = mutableListOf<Interceptor>()

    @JvmSynthetic internal val networkInterceptors = mutableListOf<Interceptor>()
    @JvmSynthetic internal val gsonTypeAdapters = mutableListOf<GsonTypeAdapter>()

    @JvmSynthetic internal var certificate: Certificate? = null
    @JvmSynthetic internal var isTrustingAllCertificates: Boolean = false

    @JvmSynthetic internal var subscribeScheduler: Scheduler? = null

    @JvmSynthetic internal var gson: Gson? = null

    /**
     * Set map of headers that will be added in the request
     */
    fun setHeaders(headers: Map<String, String>): RestBuilder {
        return this.apply { this.headers = headers }
    }

    /**
     * Set the API base URL.
     */
    fun setBaseUrl(baseUrl: String): RestBuilder {
        return this.apply { this.baseUrl = baseUrl }
    }

    /**
     * Set token header field (default if "Authorization") and token format (default is "Bearer %s")
     */
    fun setupTokenHeader(tokenHeaderField: String, tokenHeaderFormat: String): RestBuilder {
        return this.apply {
            this.tokenHeaderField = tokenHeaderField
            this.tokenHeaderFormat = tokenHeaderFormat
        }
    }

    /**
     * Add okhttp3.Interceptor
     */
    fun addInterceptor(interceptor: Interceptor, type: InterceptorType = InterceptorType.APPLICATION): RestBuilder {
        return this.apply {
            when (type) {
                RestBuilder.InterceptorType.APPLICATION -> applicationInterceptors.add(interceptor)
                RestBuilder.InterceptorType.NETWORK -> networkInterceptors.add(interceptor)
            }
        }
    }

    /**
     * Add custom serialization or deserialization.
     * Doesn't work with setGson() - Throws IllegalStateException
     *
     * @param type the type definition for the type adapter being registered
     * @param typeAdapter This object must implement at least one of the TypeAdapter,
     * InstanceCreator, JsonSerializer, and a JsonDeserializer interfaces.
     */
    fun addGsonTypeAdapter(type: Type, typeAdapter: Any): RestBuilder {
        if (gson != null) {
            throw IllegalStateException("Don't add TypeAdapters along with setting Gson")
        }
        return this.apply {
            gsonTypeAdapters.add(GsonTypeAdapter(type, typeAdapter))
        }
    }

    /**
     * Set logging level.
     * Available options:
     *      NONE: No logs. (default)
     *      BASIC: Logs request and response lines.
     *      HEADERS: Logs request and response lines and their respective headers.
     *      BODY: Logs request and response lines and their respective headers and bodies (if present).
     */
    fun setLoggingLevel(loggingLevel: LoggingLevel): RestBuilder {
        return this.apply { this.loggingLevel = loggingLevel }
    }

    /**
     * set Http Error Handler.
     * Method handleError(HTTPError) is called after receiving Response<?> from server, if Response<?> has isSuccessful == false
     *
     * used with okhttp3.Interceptor
     */
    fun handleHttpErrors(errorHandler: HttpErrorHandler): RestBuilder {
        return this.apply { httpErrorHandler = errorHandler }
    }

    /**
     * set Http Error Handler using block.
     * block is called after receiving Response<?> from server, if Response<?> has isSuccessful == false
     *
     * used with okhttp3.Interceptor
     */
    fun handleHttpErrors(block: (HttpError) -> Unit): RestBuilder {
        return this.apply {
            httpErrorHandler = object : HttpErrorHandler {
                override fun handleError(httpError: HttpError) {
                    block.invoke(httpError)
                }
            }
        }
    }

    /**
     * set Network Error Handler
     * method handleError(NetworkError) is called when IOException was caught while proceeding request
     *
     * used with okhttp3.Interceptor
     */
    fun handleNetworkErrors(errorHandler: NetworkErrorHandler): RestBuilder {
        return this.apply { networkErrorHandler = errorHandler }
    }

    /**
     * set Network Error Handler using block.
     * method handleError(NetworkError) is called when IOException was caught while proceeding request
     *
     * used with okhttp3.Interceptor
     */
    fun handleNetworkErrors(block: (NetworkError) -> Unit): RestBuilder {
        return this.apply {
            networkErrorHandler = object : NetworkErrorHandler{
                override fun handleError(error: NetworkError) {
                    block.invoke(error)
                }
            }
        }
    }

    /**
     * Set write and read timeouts by calling readTimeout() and writeTimeout() on OkHttpClient.Builder
     * Default 30 sek for both read and write
     */
    fun setupTimeouts(write: Long, read: Long, unit: TimeUnit) : RestBuilder {
        return this.apply {
            writeTimeout = write
            readTimeout = read
            timeoutTimeUnit = unit
        }
    }

    /**
     * Set the certificate to be trusted
     */
    fun setCertificate(certificate: Certificate) : RestBuilder {
        return this.apply {
            this.certificate = certificate
        }
    }

    /**
     * Set to true, to trust all certificates (default: false)
     */
    fun trustAllCertificates(trust: Boolean) : RestBuilder {
        return this.apply {
            isTrustingAllCertificates = trust
        }
    }

    /**
     * Set scheduler to subscribeOn Rx calls (default: Schedulers.io() )
     */
    fun setSubscribeScheduler(scheduler: Scheduler) : RestBuilder {
        return this.apply { subscribeScheduler = scheduler }
    }

    /**
     * Set configured Gson object
     * Doesn't work with addGsonTypeAdapter() - Throws IllegalStateException
     */
    fun setGson(gson: Gson) : RestBuilder {
        if (gsonTypeAdapters.isNotEmpty()) {
            throw IllegalStateException("Don't add TypeAdapters along with setting Gson")
        }
        return this.apply { this.gson = gson }
    }

    fun build(): RestManager {
        return RestManagerImpl(this)
    }

    enum class InterceptorType {
        APPLICATION,
        NETWORK
    }

    data class GsonTypeAdapter(
            val type: Type,
            val adapter: Any
    )

    enum class LoggingLevel(val httpLoggingLevel: HttpLoggingInterceptor.Level) {
        NONE(HttpLoggingInterceptor.Level.NONE),
        BASIC(HttpLoggingInterceptor.Level.BASIC),
        HEADERS(HttpLoggingInterceptor.Level.HEADERS),
        BODY(HttpLoggingInterceptor.Level.BODY)
    }

}