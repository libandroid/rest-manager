package pl.bms.network

import android.annotation.SuppressLint
import pl.bms.network.interceptor.AddHeadersInterceptor
import pl.bms.network.interceptor.ErrorHandlerInterceptor
import pl.bms.network.interceptor.TokenInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyStore
import java.security.cert.Certificate
import java.util.*
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 04.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
internal class RestManagerImpl internal constructor(private val builder: RestBuilder) : RestManager {

    override fun refreshToken(token: String?) {
        tokenInterceptor.token = token
    }

    override fun newCall(request: Request): Call {
        return okHttpClient.newCall(request)
    }

    private val tokenInterceptor: TokenInterceptor = TokenInterceptor(builder.tokenHeaderField, builder.tokenHeaderFormat)
    private val headersInterceptor: AddHeadersInterceptor = AddHeadersInterceptor(builder.headers)
    private val loggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply { level = builder.loggingLevel.httpLoggingLevel }
    private val errorHandlerInterceptor: ErrorHandlerInterceptor = ErrorHandlerInterceptor(builder.httpErrorHandler, builder.networkErrorHandler)

    private val okHttpClient: OkHttpClient
    internal val retrofit: Retrofit
    override val apiProvider: ApiProvider
    override val errorConverter: ErrorConverter

    init {
        okHttpClient = configureOkHttp()
        retrofit = configureRetrofit()
        errorHandlerInterceptor.retrofit = retrofit
        apiProvider = ApiProvider(retrofit)
        errorConverter = ErrorConverter(retrofit)
    }


    private fun configureRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(builder.baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(builder.gson ?: createGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory
                        .createWithScheduler(builder.subscribeScheduler ?: Schedulers.io()))
                .build()
    }

    private fun configureOkHttp(): OkHttpClient{
        val clientBuilder = OkHttpClient.Builder()
                .readTimeout(builder.readTimeout, builder.timeoutTimeUnit)
                .writeTimeout(builder.writeTimeout, builder.timeoutTimeUnit)
                .addInterceptor(headersInterceptor)
                .addInterceptor(tokenInterceptor)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(errorHandlerInterceptor)

        builder.applicationInterceptors.forEach { interceptor ->
            clientBuilder.addInterceptor(interceptor)
        }
        builder.networkInterceptors.forEach { interceptor ->
            clientBuilder.addNetworkInterceptor(interceptor)
        }
        if (builder.isTrustingAllCertificates) {
            val trustManager = createTrustManagerForAllCerts()
            clientBuilder.sslSocketFactory(createSSLSocketFactory(trustManager), trustManager)
                    .hostnameVerifier { s, sslSession -> true } //TODO: verify
        } else if (builder.certificate != null) {
            val trustManager = createTrustManagerForCertificate(builder.certificate!!)
            clientBuilder.sslSocketFactory(createSSLSocketFactory(trustManager), trustManager)
                    .hostnameVerifier { s, sslSession -> true } //TODO: verify
        }
        return clientBuilder.build()
    }

    private fun createTrustManagerForAllCerts(): X509TrustManager {
        return object : X509TrustManager {
            @SuppressLint("TrustAllX509TrustManager")
            @Throws(java.security.cert.CertificateException::class)
            override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
            }

            @SuppressLint("TrustAllX509TrustManager")
            @Throws(java.security.cert.CertificateException::class)
            override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
            }

            override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                return arrayOf()
            }
        }
    }

    private fun createTrustManagerForCertificate(certificate: Certificate): X509TrustManager {
        // creating a KeyStore containing our trusted CAs
        val keyStoreType = KeyStore.getDefaultType()
        val keyStore = KeyStore.getInstance(keyStoreType)
        keyStore.load(null, null)
        keyStore.setCertificateEntry("certificate", certificate)

        // creating a X509TrustManager that trusts the certificates in our KeyStore
        // https://square.github.io/okhttp/3.x/okhttp/okhttp3/OkHttpClient.Builder.html#sslSocketFactory-javax.net.ssl.SSLSocketFactory-javax.net.ssl.X509TrustManager-
        val trustManagerFactory = TrustManagerFactory.getInstance(
                TrustManagerFactory.getDefaultAlgorithm())
        trustManagerFactory.init(keyStore)
        val trustManagers = trustManagerFactory.trustManagers
        if (trustManagers.size != 1 || trustManagers[0] !is X509TrustManager) {
            throw IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers))
        }
        return trustManagers[0] as X509TrustManager
    }

    private fun createSSLSocketFactory(trustManager: X509TrustManager): SSLSocketFactory {
        // creating an SSLSocketFactory that uses our TrustManager
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, arrayOf(trustManager), null)
        return sslContext.socketFactory
    }

    private fun createGson(): Gson {
        val gsonBuilder = GsonBuilder()
        builder.gsonTypeAdapters.forEach { typeAdapter ->
            gsonBuilder.registerTypeAdapter(typeAdapter.type, typeAdapter.adapter)
        }
        return gsonBuilder.create()
    }

}