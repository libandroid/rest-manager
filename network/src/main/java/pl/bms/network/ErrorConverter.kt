package pl.bms.network

import pl.bms.network.error.ApiError
import retrofit2.Response
import retrofit2.Retrofit


/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 17.05.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
class ErrorConverter (private val retrofit: Retrofit){

    /**
     * Convert Response to selected Type, or return null
     */
    fun <T> convertError(type: Class<T>, response: Response<*>): T? {
        return response.errorBody()?.let { body ->
            retrofit.responseBodyConverter<T>(type, arrayOfNulls(0))?.convert(body)
        }
    }

    /**
     * Convert Response to ApiError or return null
     */
    fun convertError(response: Response<*>): ApiError? {
        return response.errorBody()?.let { body ->
            retrofit.responseBodyConverter<ApiError>(ApiError::class.java, arrayOfNulls(0))?.convert(body)
        }
    }

}