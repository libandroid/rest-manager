package pl.bms.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 03.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
internal class TokenInterceptor(private val tokenHeaderField: String, private val tokenHeaderFormat: String) : Interceptor{

    var token: String? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val requestBuilder = request.newBuilder()
        token?.also { token ->
            requestBuilder.addHeader(tokenHeaderField, String.format(tokenHeaderFormat, token))
        }
        return chain.proceed(requestBuilder.build())
    }

}