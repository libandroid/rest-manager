package pl.bms.network.interceptor

import android.os.Handler
import android.os.Looper
import pl.bms.network.error.HttpError
import pl.bms.network.error.HttpErrorHandler
import pl.bms.network.error.NetworkError
import pl.bms.network.error.NetworkErrorHandler
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Retrofit
import java.io.IOException

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 05.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
internal class ErrorHandlerInterceptor(private val httpErrorHandler: HttpErrorHandler?, private val networkErrorHandler: NetworkErrorHandler?) : Interceptor {

    internal var retrofit: Retrofit? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        try {
            val response = chain.proceed(request)
            if (!response.isSuccessful && httpErrorHandler != null) {
                val mainHandler = Handler(Looper.getMainLooper())
                val myRunnable = Runnable {
                    val httpError = HttpError(response)
                    httpErrorHandler.handleError(httpError)
                }
                mainHandler.post(myRunnable)
            }
            return response

        } catch (exception: IOException) {
            networkErrorHandler?.also {
                val mainHandler = Handler(Looper.getMainLooper())
                val myRunnable = Runnable {
                    val networkError = NetworkError(request, exception)
                    networkErrorHandler.handleError(networkError)
                }
                mainHandler.post(myRunnable)
            }
            throw exception
        }

    }

}