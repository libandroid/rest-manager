package pl.bms.network

import retrofit2.Retrofit

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 12.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
class ApiProvider(private val retrofit: Retrofit) {

    private val apis = mutableMapOf<String, Any>()

    /**
     * create api with retrofit.create() method, or return already created from cache
     */
    fun <T: Any> getApi(type: Class<T>) : T {
        (apis[type.canonicalName] as? T)?.also {
            return it
        }
        return retrofit.create(type).also { api ->
            apis[type.canonicalName] = api
        }
    }

}