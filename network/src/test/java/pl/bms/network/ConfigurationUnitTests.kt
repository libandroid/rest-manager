package pl.bms.network

import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 14.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class ConfigurationUnitTests {

    private lateinit var builder: RestBuilder
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun setup() {
        builder = RestBuilder().apply {
            baseUrl = "http://example.com"
            subscribeScheduler = Schedulers.trampoline()
        }
        mockWebServer = MockWebServer().apply {
            start()
        }
    }

    @Test
    fun testConfigureBaseUrl() {
        val url = "http://test.com/"

        val manager = builder.setBaseUrl(url).build()

        assertEquals(url, (manager as RestManagerImpl).retrofit.baseUrl().toString())
    }

    @Test
    fun testHeadersConfiguration() {
        val testHeaderKey = "testFieldName"
        val testHeaderValue = "testFieldValue"
        val testHeaderKey2 = "testFieldName2"
        val testHeaderValue2 = "testFieldValue2"
        val headers = mapOf( testHeaderKey to testHeaderValue, testHeaderKey2 to testHeaderValue2)

        val manager = builder.setHeaders(headers)
                .build()

        val request = makeTestCall(manager)

        assertEquals(testHeaderValue, request.getHeader(testHeaderKey))
        assertEquals(testHeaderValue2, request.getHeader(testHeaderKey2))

        mockWebServer.shutdown()
    }

    @Test
    fun testTokenConfiguration() {
        val tokenKey = "TestAuthorization"
        val tokenFormat = "TestToken %s"
        val firstToken = "f43gf8734gf873y4dh"
        val secondToken = "vhefuh37f84yfhg738"

        val manager = builder.setupTokenHeader(tokenKey, tokenFormat)
                .build()

        val firstRequest = makeTestCall(manager)
        assert(firstRequest.getHeader(tokenKey) == null)

        manager.refreshToken(firstToken)
        val secondRequest = makeTestCall(manager)
        assertEquals(secondRequest.getHeader(tokenKey), String.format((tokenFormat), firstToken))

        manager.refreshToken(secondToken)
        val thirdRequest = makeTestCall(manager)
        assertEquals(thirdRequest.getHeader(tokenKey), String.format((tokenFormat), secondToken))

        mockWebServer.shutdown()
    }

    private fun makeTestCall(manager: RestManager): RecordedRequest {
        mockWebServer.enqueue(MockResponse())
        manager.newCall(Request.Builder().url(mockWebServer.url("/")).build()).execute()
        return mockWebServer.takeRequest()
    }

    @Test
    fun testAddNetworkInterceptor() {
        val testKey = "addInterceptorTestKey"
        val testValue = "addInterceptorTestValue"

        val interceptor = Interceptor { chain ->
            val request = chain.request()
            val requestBuilder = request.newBuilder()
            requestBuilder.addHeader(testKey, testValue)
            chain.proceed(requestBuilder.build())
        }

        val manager = builder
                .addInterceptor(interceptor, RestBuilder.InterceptorType.NETWORK)
                .build()

        val request = makeTestCall(manager)
        assertEquals(request.getHeader(testKey), testValue)

        mockWebServer.shutdown()
    }

    @Test
    fun testAddApplicationInterceptor() {
        val testKey = "addInterceptorTestKey"
        val testValue = "addInterceptorTestValue"

        val interceptor = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val request = chain.request()
                val requestBuilder = request.newBuilder()
                requestBuilder.addHeader(testKey, testValue)
                return chain.proceed(requestBuilder.build())
            }
        }

        val manager = builder
                .addInterceptor(interceptor, RestBuilder.InterceptorType.APPLICATION)
                .build()

        val request = makeTestCall(manager)
        assertEquals(request.getHeader(testKey), testValue)

        mockWebServer.shutdown()
    }

    @Test(expected = SocketTimeoutException::class)
    fun testSetTimeouts() {

        val manager = builder.setupTimeouts(50, 50, TimeUnit.MILLISECONDS)
                .build()

        mockWebServer.enqueue(MockResponse().setBodyDelay(100, TimeUnit.MILLISECONDS))
        manager.newCall(Request.Builder().url(mockWebServer.url("/")).build()).execute()
        mockWebServer.takeRequest()
    }


}